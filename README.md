<!-- Comments are wrapped as here.  These are not displayed as HTML.
Note that the markdown "reference style" is used.  Hence, reference
links are enclosed in square brackets [ ] instead of parenthesis (
). -->

<!-- README header -->
<br />
<div align="center">
  <a href="https://gitlab.com/gcl-nuig/sops">
    <img src=".attachments/gcl-logo.png" alt="Logo" width="145" height="120">
  </a>

  <h3 align="center">Standing Operational Procedures (SOPs)</h3>
  <p align="center">
    Gavin Collins Laboratory Group standing operational procedures.
    <br />
    <br />
    <a href="https://gitlab.com/gcl-nuig/sops/-/issues/new"><strong>Require or submit an SOP </strong></a>
    <br />
    <br />
    or 
    <a href="https://gitlab.com/gcl-nuig/procurement/-/issues">Explore the Standing Operational Procedures</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#guidelines">Guidelines</a></li>
    <li><a href="#general-laboratory-safety">General Laboratory Safety</a></li>
    <li><a href="#sample-collection-and-storage">10) Sample Collection and Storage</a></li>
    <li><a href="#general-microbiology">20) General Microbiology</a></li>
    <li><a href="#molecular-biology">30) Molecular Biology</a></li>
    <li><a href="#water-and-wastewater">40) Water and Wastewater</a></li>
    <li><a href="#anaerobic-sludge">50) Anaerobic Sludge</a></li>
    <li><a href="#light-and-electron-microscopy">60) Light and Electron Microscopy</a></li>
    <li><a href="#useful-resources">Useful Resources</a></li>
    <li><a href="#contact">Contacts</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- PROCUREMENT VOCABULARY -->
## Guidelines

Standing operational procedures (SOPs) are produced in either
[Org](https://orgmode.org/) or [GFM](https://github.github.com/gfm/)
markup language.  These are thus exported to PDF and HTML format using
[Pandoc](https://pandoc.org/) or the built-in [Org export
engine](https://orgmode.org/worg/dev/org-export-reference.html) for
consultation.

SOP file names are always of the structure
`SOP-{CAT}-{INDEX}--{Short-description}.{EXT}`, where `{CAT}` is the
category code, `{INDEX}` is an incremental index of an SOP in such
category, the `{Short-description}` is a 5-word-max description of the
procedure goals, and finally `{EXT}` is the file extension.

The current categories are:

 - **10**: Sample Collection and Storage
   - **11**: Sample Collection
   - **12**: Sample Shipping and Receiving
   - **13**: Sample Storage
 - **20**: General Microbiology
   - **21**: Pure Cultures
 - **30**: Molecular Biology
   - **31**: PCR-Based Methods
   - **32**: Gel Electrophoresis
   - **33**: Illumina Sequencing
   - **34**: ONT MinION Sequencing
 - **40**: Water and Wastewater
   - **41**: Physico-Chemical Properties
   - **42**: Metals
   - **43**: Inorganic Nonmetallic Constituents
   - **44**: Organic Constituents
 - **50**: Anaerobic Sludge
   - **51**: Physico-Chemical Properties
   - **52**: Metals
   - **53**: Inorganic Nonmetallic Constituents
   - **55**: Organic Constituents
   - **56**: Biological Activity
   - **57**: Nucleic Acid Extraction
 - **60**: Light and Electron Microscopy
   - **61**: Light Microscopy (general)
   - **62**: Flourescent Light Microscopy
   - **63**: Scanning Electron Microscopy

## General Laboratory Safety

[Oklahoma State University example](https://ehs.okstate.edu/general-laboratory-safety-rules.html)

## Sample Collection and Storage
## General Microbiology
## Molecular Biology
## Water and Wastewater

 - **Total, Fixed, and Volatile Solids in Water, Solids, and
   Biosolids.  EPA Methods 1684** ([pdf](https://www.epa.gov/sites/default/files/2015-10/documents/method_1684_draft_2001.pdf))

## Anaerobic Sludge

 - **Specific Methanogenic Activity** (![html](.exports/SOP-56-1--Specific-methanogenic-activity-assay.html), ![pdf](.exports/SOP-56-1--Specific-methanogenic-activity-assay.pdf))
 - **Total, Fixed, and Volatile Solids in Water, Solids, and
   Biosolids.  EPA Methods 1684** ([pdf](https://www.epa.gov/sites/default/files/2015-10/documents/method_1684_draft_2001.pdf))

## Light and Electron Microscopy
## Useful resources

 - ![Standard Methods For The Examination of Water and Wastewater](.attachments/APHA-23ed-Standard-methods-examination-water-wastewater.pdf)

## Contact
## Acknowledgments

 - [ReadTheOrg CSS theme, by Fabrice Niessen](https://github.com/fniessen/org-html-themes "ReadTheOrg")
