#+title: Sample collection at full-scale IC reactor(s) for the Trans2Waste project
#+subtitle: Collection of anaerobic granular sludge from N1 and N3
#+author: Marco Prevedello
#+email: m.prevedello1@nuigalway.ie
#+date: [2020-12-22 Tue]
#+category: 1000-sampling

* Foreword
Standing operation procedure (SOP) reports authored by a member of the GCL
(Gavin Collins Laboratory) describe step-by-step procedures and methodologies
used within the GCL at the National University of Ireland Galway (Department of
Microbiology).

These procedure are (generally) reviewed by Gavin Collins and a copy is provided
on request.

* Scope
Sample collection or anaerobic granular sludge (in its mixed liquor) from the
bottom (N1, h = 1.5 m) and middle (N3, h = 9.1 m) layers of the sludge bed of
two (2) PAQUES internal circulation (BIOPAQ®IC) reactors for the /Trans2Waste/
project. The scope of this standing operation procedure (SOP) is to collect a
portion of material small enough in volume to be transported from the industrial
site (Kalundborg, Denmark) to the National University of Ireland Galway
(Ireland) and yet large enough for analytical purposes while still accurately
representing the material being sampled. This imply that the relative
proportions or concentrations of all pertinent components will be the same in
the samples as in the material being sampled, and that the sample will be
handled in such a way that no significant changes in composition occur before
the tests are made. (Quote from APHA Standard Methods 23rd edition, section
1060, page 1)

*WARNING* This protocol is still in testing phase, and the impact on downstream
analysis is not validate.

* Normative references
The following referenced documents are indispensable for the
application of this document. For dated references, only the edition
cited applies. For undated references, the latest edition of the
referenced document (including any amendments) applies.

  - *No current documents required*

The following back-link documents refer to this SOP.

  - Nucleic acid extraction SOP
  - Tessier modified sequential extraction SOP
  - /aqua regia/ MAD extraction SOP
  - Total, suspended, and dissolved solid analysis SOP
  - Granular size distribution SOP
  - Specific methanogenic activity SOP
    
* Terms and definitions
For the purposes of this SOP, the following definitions apply.

  - Replicates :: (Also /Replicas/) 1) Replicate sample. That is, two or more
    samples taken at the same time from one location (field replicate). 2)
    (Technical) Replicate. That is, two or more aliquot of the same sample
    processed equally for a laboratory analysis. For example, two aliquots
    processed separately for microwave-assisted digestion. 3) Extract analysis
    replicate. That is, two or more analyses for the same constituent in a
    single extract of one sample. For example, triplicate reading of one
    technical replica at the ICP-MS.
  - Fortification :: Adding a known quantity of analyte to a sample or blank to
    increase the analyte concentration, usually for the purpose of comparing to
    test result on the unfortified sample and estimating percent recovery or
    matrix effects on the test to assess accuracy.
  - Internal standard :: A pure compound added to a sample extract just before
    instrumental analysis to permit correction for inefficiencies.
  - Laboratory control standard :: A standard usually certified by an outside
    agency that is used to measure the bias in a procedure. For certain
    constituents and matrices.
  - Spike :: See /Fortification/

* Safety remarks
*WARNING* - Sewage sludges may contain potentially pathogenic organisms,
micro-pollutant, heavy metals, or other hazardous substances. Therefore
appropriate precautions must be taken when handling such sludges.

*WARNING* - Toxic test chemicals and those whose properties are not known must be
handled with care.

* Principle
The result of any downstream analysis performed in the setting of the
/Trans2Waste/ project is highly dependent on the quality of the sample collection
procedure.

* Interferences and sources of errors
* Reagents
* Apparatus
* Sampling and sample pretreatment
* Procedure

We suggest that sampling take place simultaneously with the normal plant
monitoring carried out at Novozymes KA. This would both facilitate the sampling
procedure, since it shares several steps with the normal plant monitoring, but
also (and especially) it would enable the appropriate comparison of data from
our analyses with the routine performance monitoring data.

Along with the sampling procedure we kindly ask you to answer a brief
questionnaire that will be provided with each sampling kit. An example of this
questionnaire is provided below.

*** Sampling kit contents
    :PROPERTIES:
    :CUSTOM_ID: sampling-kit-contents
    :END:
Please ensure that the sampling material we have sent to you includes the
following, and has not been damaged in transit:

  + Tube storage box for 50-mL tubes
  + Eighteen (18) labeled, 50-mL tubes, each containing 10 mL of non-hazardous,
    non-toxic, liquid preservative /RNAlater/™ (transparent liquid). (These can
    be stored at ambient temperature until the sample material is added -- see
    the protocol later.)
  + +Twenty-five (25) large-volume transfer pipettes (you will use 18, but we
    have provided some spare)+ Eight modified large bore 20 mL syringes.
  + Cooling elements (Instant ice packs)
  + Eighteen (18) labeled, 125-mL sampling bottles
  + Soft packing material
  + +Sample sheet and questionnaire+
  + Sampling, processing and shipping summary-protocols (only provided with the
    first sampling kit)

*** Sampling
    :PROPERTIES:
    :CUSTOM_ID: sampling
    :END:
Three independent replicate samples (*A*, *B* and *C*) should be drawn separately from
each sampling point (*Inf-KA1*, *Inf-KA2*, *N3-KA1*, N3-KA2*, *Eff-KA1* and *Eff-KA2)*-
and treated as individual samples (i.e. not mixed together). /All of the
locations should be sampled on the same day./

Thus, the full list of samples at each time will be:

| Inf-KA1-A | N3-KA1-A | ... |   |   | Eff-KA2-A |
|-----------+----------+-----+---+---+-----------|
|           |          |     |   |   |           |
|           |          |     |   |   |           |
|           |          |     |   |   | Eff-KA2-C |

The following procedure should be carried out at each sampling point of each
reactor:

**** 1) Sample Collection (From the reactors KA1 and KA2)
     :PROPERTIES:
     :CUSTOM_ID: sample-collection-from-the-reactors-ka1-and-ka2
     :END:

 1) Open the valve and discard stagnant volume from pipes and hoses. (Discard at
    least 1 L)
 2) In-house sample: Open the valve again and collect /in-house/ sample into a
    suitable container (not provided). This sample should be used for the normal
    monitoring of the reactor status.
 3) Sample A: Open valve again and collect the sample directly into the labeled
    125-mL container provided. Fill the container as much as possible to
    minimize aeration. (/This is the first of the three replicates./)
 4) Sample B: Open valve again and transfer the sample into another one of the
    labeled 125-mL containers provided. (/This is the second of the three
    replicates./)
 5) Sample C: Open valve again and transfer the sample into another of the
    labeled 125-mL containers provided. (/This is the third of the three
    replicates./).

**** 2) Sample Processing (At the in-house analytical laboratory)
     :PROPERTIES:
     :CUSTOM_ID: sample-processing-at-the-in-house-analytical-laboratory
     :END:

 1) Sample A: Using a *new* disposable pipette from those provided, transfer
    approximately 10 mL from sample A (*Inf-KA1-A*, *N3-KA1-A*, *Eff-KA1-A*,
    *Inf-KA2-A*, *N3-KA2-A* /or/ *Eff-KA2-A*) to the *respective*, labeled, 50-mL tube
    provided (labeled as *Seq-Inf-KA1-A*, *Seq-N3-KA1-A*, *Seq-Eff-KA1-A*,
    *Seq-Inf-KA2-A*, *Seq-N3-KA2-A* /or/ *Seq-Eff-KA2-A*).The tubes already each contain
    10 mL of non-hazardous, non-toxic, liquid preservative, /RNAlater/™). /Tightly
    close the 50 mL tubes./
 2) Sample B: Using a *new* disposable pipette from those provided, transfer
    approximately 10 mL from sample B (*Inf-KA1-B*, *N3-KA1-B*, ...) to the
    *respective*, labeled, 50-mL tube provided (labeled as *Seq-Inf-KA1-B*,
    *Seq-N3-KA1-B*, ...).The tubes already each contain 10 mL of non-hazardous,
    non-toxic, liquid preservative, /RNAlater/™). /Tightly close the 50 mL tubes./
 3) Sample C: Using a *new* disposable pipette from those provided, transfer
    approximately 10 mL from sample C (*Inf-KA1-C*, *N3-KA1-C*, ...) to the
    *respective*, labeled, 50-mL tube provided (labeled as *Seq-Inf-KA1-C*,
    *Seq-N3-KA1-C*, ...).The tubes already each contain 10 mL of non-hazardous,
    non-toxic, liquid preservative, /RNAlater/™). /Tightly close the 50 mL tubes./
 4) Seal the cap of each tube using the Parafilm™ provided.
 5) Mix each tube by end-over-end inversion a couple of times to ensure good
    contact between the /RNAlater/™ and the sample.

*** Shipping (to be done on day of shipping)
    :PROPERTIES:
    :CUSTOM_ID: shipping-to-be-done-on-day-of-shipping
    :END:

 1) Place the 125-mL bottles into the *shipping box* provided.
 2) Place the 50-mL tubes into the *tube storage box*. Place this into the
    *shipping box*.
 3) Activate the cooling *instant ice pack* (follow instruction on the pack) and
    place them inside the *shipping box*.
 4) Fill the empty spaces with the *packing material* provided.
 5) Seal the shipping box with tape.
 6) Arrange the shipment with you courier of choice. The destination address is
    already printed on the shipping box. When shipping choose priority shipping
    and be sure to require a tracking number.

Please notify us when the samples have been sent; attaching the *tracking number,*
and the completed *sample sheet* and *questionnaire*.

*** Questionnaire
    :PROPERTIES:
    :CUSTOM_ID: questionnaire
    :END:

 1. Have the operation conditions been unstable during the last couple of days
    prior to sampling (operation stops, foaming, excessive use of chemicals or
    other irregularities, any of which may impact the analysis)?
 2. Did any of the sample present unusual visual characteristics? (e.g., unusual
    color, size, smell, disintegration)
 3. Did any of the samples was damaged?
 4. What are the obtained measurements of settling material (Imhoff cone) and
    solid content (TS, VS)?

*Project:* Collaboration with Novozymes. Micro-nutrients and Iron supplementation
for improved biomass growth in granular sludge IC reactors

*Version*: 20200129

*Author*: Marco Prevedello

*Sample Key:*
| KA1 Reactor            | KA2 Reactor            |
|------------------------+------------------------|
| KA1 Influent = *KA1-Inf* | KA2 Influent = *Inf-KA2* |
| KA1 Niveau 3 = *KA1-N3*  | KA2 Niveau 3 = *KA2-N3*  |
| KA1 Effluent = *KA1-Eff* | KA2 Effluent = *KA2-Eff* |

*** Sampling Protocol for Routine Samples (RS)
    :PROPERTIES:
    :CUSTOM_ID: sampling-protocol-for-routine-samples-rs
    :END:
**** Back side
*REPEAT THIS AT EACH OF THE SIX* sampling points: *KA1-Inf*; *KA1-N3*; KA1-Eff*;
**KA2-Inf*; *KA2-N3; KA2-Eff*. Refer to the full protocol document if in doubt (or
see reverse of this card).

  1. *Clear dead volume:* Open valve and discard at least 1 L of stagnant volume.
  2. *Collect in-house sample:* Open valve and collect in-house sample to a
     suitable container (not provided). This sample is for in-house monitoring
     of the reactor.
  3. *Collect sample ‘A'*: Open valve again and collect sample into the 125-mL
     container (labeled *KA1-Inf-A*, *N3-KA1-A*, etc...). Fill the container as much
     as possible.
  4. *Collect sample ‘B'*: Open valve again and collect sample into the 125-mL
     container (labeled *KA1-Inf-B*, *N3-KA1-B*, etc...). Fill the container as much
     as possible.
  5. *Collect sample ‘C'*: Open valve again and collect sample into the 125-mL
     container (labeled *KA1-Inf-C*, *N3-KA1-C*, etc...). Fill the container as much
     as possible.
  6. *Please now check the sample:* You should have the following:

**** Front side
2) Sample Processing Protocol

*Express Version for Routine Samples*

*Project:* Collaboration with Novozymes. Micro-nutrients and Iron supplementation
for improved biomass growth in granular sludge IC reactors

*Version*: 20200129

*Author*: Marco Prevedello

*Sample key:*

| KA1 Reactor                           | KA2 Reactor                           |
|---------------------------------------+---------------------------------------|
| Sequencing KA1 Influent = *Seq-KA1-Inf* | Sequencing KA2 Influent = *Seq-Inf-KA2* |
| Sequencing KA1 Niveau 3 = *Seq-KA1-N3*  | Sequencing KA2 Niveau 3 = *Seq-KA2-N3*  |
| Sequencing KA1 Effluent = *Seq-KA1-Eff* | Sequencing KA2 Effluent = *Seq-KA2-Eff* |

*** Processing Protocol for Routine Samples (RS)
    :PROPERTIES:
    :CUSTOM_ID: processing-protocol-for-routine-samples-rs
    :END:
**** Back side
*REPEAT THIS* *FOR EACH OF THE EIGHTEEN (18)* samples collected in the 125-mL
containers. Refer to the full protocol document if in doubt.

  1. *Transfer ‘A' Samples:* Using a new disposable pipette, transfer
     approximately 10 mL from the 125-mL container (e.g., *KA1-Inf-A*, *N3-KA1-A*,
     etc...) to the corresponding, labeled, 50-mL tube (labeled as
     *Seq-KA1-Inf-A*, *Seq-N3-KA1-A*, etc...). Tightly close the 50 mL tube.
  2. *Transfer ‘B' Samples:* Using a new disposable pipette, transfer
     approximately 10 mL from the 125-mL container (e.g., *KA1-Inf-B*, *N3-KA1-B*,
     etc...) to the corresponding, labeled, 50-mL tube (labeled as
     *Seq-KA1-Inf-B*, *Seq-N3-KA1-B*, etc...). Tightly close the 50 mL tube.
  3. *Transfer ‘C' Samples:* Using a new disposable pipette, transfer
     approximately 10 mL from the 125-mL container (e.g., *KA1-Inf-C*, *N3-KA1-C*,
     etc...) to the corresponding, labeled, 50-mL tube (labeled as
     *Seq-KA1-Inf-C*, *Seq-N3-KA1-C*, etc...). Tightly close the 50 mL tube.
  4. *Secure the Tubes:* Ensure that each 50-mL tube is closed. Seal the caps
     using Parafilm™.
  5. *Mix: Invert each tube end-over-end* a couple of times, to mix sample and
     /RNAlater/™.
  6. *Please now check the samples: You should have the samples reported in the
     following table.*
  7. *Store samples.* Place all samples at 4°C until shipping. Do not freeze.

**** Front side
3) Samples Shipping Protocol

Express Version for Shipping of Routine Samples

  + Project :: Collaboration with Novozymes. Micro-nutrients and Iron
    supplementation for improved biomass growth in granular sludge IC reactors
  + Version :: 20200129
  + Author :: Marco Prevedello
  + Shipping Address :: NUIG Microbiology, University road, Galway, Ireland

* Quality control
* Test report
* Tips and tricks
* Troubleshooting
* Bibliography


